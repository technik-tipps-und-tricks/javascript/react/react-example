import React from 'react';
import { DefaultProps } from '../../types/default';
import styled from 'styled-components';
import NavLink from './NavLink';
import { Button } from 'react-bootstrap';

export interface NavLinkButtonProps extends DefaultProps {
  id: string;
  to: string;
  active: boolean;
  onClick: (id: string) => void;
}

const NavButton = styled(Button)`
  margin: .5em;
  width: 9em;
`;

const activeLayout = "primary";
const inactiveLayout = "outline-primary";

const NavLinkButton = (props: NavLinkButtonProps): React.FunctionComponentElement<NavLinkButtonProps> => {

  const onClickNavButton = () => {
    props.onClick(props.id);
  }

  return (
    <NavLink to={props.to}>
      <NavButton variant={props.active ? activeLayout : inactiveLayout} onClick={onClickNavButton}>{props.children}</NavButton>
    </NavLink>
  );
}

NavLinkButton.defaultProps = {
  id: "",
  to: "/",
  active: false,
  onClick: null,
}

export default NavLinkButton;