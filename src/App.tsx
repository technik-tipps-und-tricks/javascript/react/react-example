import React from 'react';
import { DefaultProps } from './types/default';
import styled  from 'styled-components';

const Headline = styled.h1`
    color: white;
    text-decoration: underline;
  `;

const App = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  
  return (
    <div className="App">
      <header className="App-header">
        <Headline>Empty file!</Headline>
      </header>
    </div>
  );
}

export default App;
