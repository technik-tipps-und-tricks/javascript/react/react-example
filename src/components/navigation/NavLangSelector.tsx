import React from 'react';
import { DefaultProps } from '../../types/default';
import { useTranslation } from 'react-i18next';
import { Image } from 'react-bootstrap';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../reducers/appReducer';
import { setSelectedLanguage } from '../../actions/appActions';

/**
 * Styles
 */
const LangSelector = styled.div`
  text-align: center;
  border: 1px solid gray;
  padding: 0em .5em .6em;
  margin-left: 5px;
  max-height: 5em;
  min-width: 150px;
  max-width: 150px;
`;

const Label = styled.p`
  color: white;
  font-size: .8em;
  text-align: left;
  padding-left: calc(.5em + 24px);
  margin-left: 0em;
  margin-bottom: 0em;
  padding-bottom: .2em;
`;

const SelectorContainer = styled.div`
  /* */
`;

const FlagImage = styled(Image)`
  margin-left: .0em;
  margin-right: .2em;
`;

const Selector = styled.select`
  height: 2em;
  min-width: 8em;
  font-size: .8em;
`;

export interface NavLangSelectorProps extends DefaultProps {
    value: string;
    onSelected: (value: string) => void;
}

/**
 * NavLangSelector component
 * @param props value: "en" | "de" 
 */
const NavLangSelector = (props: NavLangSelectorProps): React.FunctionComponentElement<NavLangSelectorProps> => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const selectedLanguage = useSelector((state: { app: AppState}) => state.app.selectedLanguage);

  const onSelectedLanguageChange = (newSelectedLanguage: string): void => {
    i18n.changeLanguage(newSelectedLanguage).then((value) => {
      dispatch(setSelectedLanguage(newSelectedLanguage));
    });
    //onSelectedLanguage();
    props.onSelected(newSelectedLanguage);

  };

  

  return (
    <LangSelector>
      <Label>{t('general.language.label')}:</Label>
      <SelectorContainer>
        <FlagImage alt="flag [Image]" src={"/images/flag-" + selectedLanguage + ".png"} height="24" width="24" />
        <Selector name="langselect" id="langselect" value={selectedLanguage} onChange={e => onSelectedLanguageChange(e.target.value)}>
          <option value="en">{t('general.language.options.en')}</option>
          <option value="de">{t('general.language.options.de')}</option>
          <option value="pl">{t('general.language.options.pl')}</option>
        </Selector> 
	    </SelectorContainer>
    </LangSelector>
  );
}

/**
 * Default component props
 */
NavLangSelector.defaultProps = { value: "en"}

export default NavLangSelector;