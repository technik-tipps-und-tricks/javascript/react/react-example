import { 
  SET_SELECTED_LANGUAGE, 
  SET_ACTIVE_NAV_BUTTON_ID,
  AppActionTypes 
} from '../actions/appActions';

 export interface AppState {
  selectedLanguage: string;
  activeNavButtonId: string;
}

 const initialState: AppState = {
  selectedLanguage: 'de',
  activeNavButtonId: 'home',
};

 const appReducer = (state = initialState, action: AppActionTypes,): AppState => {
  switch (action.type) {
    case SET_SELECTED_LANGUAGE:
      return {
        ...state,
        selectedLanguage: action.payload.selectedLanguage,
      };
    case SET_ACTIVE_NAV_BUTTON_ID:
      return {
        ...state,
        activeNavButtonId: action.payload.activeNavButtonId,
      };
    default:
      return state;
  }
};

 export default appReducer;