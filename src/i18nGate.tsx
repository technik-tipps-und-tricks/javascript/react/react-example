import React from 'react';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import TranslationDE from './translations/de.json';
import TranslationEN from './translations/en.json';
import TranslationPL from './translations/pl.json';
import { DefaultProps } from './types/default';
import { useSelector } from 'react-redux';
import { AppState } from './reducers/appReducer';

const I18nGate = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  const { children } = props;
  const selectedLanguage = useSelector((state: { app: AppState}) => state.app.selectedLanguage);
  
  i18n.use(initReactI18next).init({
    resources: {
      de: {
        translation: {
          ...TranslationDE,
        },
      },
      en: {
        translation: {
          ...TranslationEN,
        },
      },
      pl: {
        translation: {
          ...TranslationPL,
        },
      },
    },
    lng: selectedLanguage,
    fallbackLng: "en",

    interpolation: {
      escapeValue: false,
    },
  });

  return (
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    { ...children }
  );
}

export default I18nGate;