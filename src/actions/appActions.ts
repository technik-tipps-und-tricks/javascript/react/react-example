export const SET_SELECTED_LANGUAGE = 'APP/SET_SELECTED_LANGUAGE';
export const SET_ACTIVE_NAV_BUTTON_ID = 'APP/SET_ACTIVE_NAV_BUTTON_ID';
export type AppActionTypes = SetSelectedLanguage | SetActiveNavButtonId;

/**
 * Selected Language
 */
interface SetSelectedLanguage {
  type: typeof SET_SELECTED_LANGUAGE;
  payload: SetSelectedLanguagePayload;
}

interface SetSelectedLanguagePayload {
  selectedLanguage: string;
}

export function setSelectedLanguage(selectedLanguage: string): SetSelectedLanguage {
  return {
    type: SET_SELECTED_LANGUAGE,
    payload: {
      selectedLanguage,
    },
  };
}

/**
 * Navigation button id
 */
interface SetActiveNavButtonId {
  type: typeof SET_ACTIVE_NAV_BUTTON_ID;
  payload: SetActiveNavButtonIdPayload;
}

interface SetActiveNavButtonIdPayload {
  activeNavButtonId: string;
}

export function setActiveNavButtonId(activeNavButtonId: string): SetActiveNavButtonId {
  return {
    type: SET_ACTIVE_NAV_BUTTON_ID,
    payload: {
      activeNavButtonId,
    },
  };
}