# React Example - Changelog

**v1.0.3** [2020-03-01]

  - added redux, react-redux, redux-persist, bootstrap and styled-components
  - added and implemented custom navigation components (buttons)
  - header style adjustment
  - language selector available
  - added Polish language
  - moved src/App.tsx to src/pages/Home.tsx and deleted src/App.tsx
  - page content view adjustment
  - App.css style moved to index.css
  - Navigation improvements
  - Bugfix on image path

**v1.0.2** [2020-02-13]

  - *i18next* and *react-i18next* installed
  - internationalisation implemented
  - English and German translation inserted

**v1.0.1** [2020-02-12]

  - simple *react-router* and additional pages implemented

**v1.0.0** [2020-02-11]
  
  - initial release
