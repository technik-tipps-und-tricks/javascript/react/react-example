import React from 'react';
import { useTranslation } from 'react-i18next';
import { DefaultProps } from '../types/default';

const NotFoundPage = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  const { t } = useTranslation();

  return (
    <div className="App">
      <header className="App-header">
        <h1>{t('notFoundPage.header.h1')}</h1>
      </header>
    </div>
  );
}

export default NotFoundPage;