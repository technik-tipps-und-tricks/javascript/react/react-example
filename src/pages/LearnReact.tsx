import React from 'react';
import { useTranslation } from 'react-i18next';
import { DefaultProps } from '../types/default';
import logo from '../logo.svg';
import styled from 'styled-components';

const Header = styled.header`
  background-color: #282c34;
  background: linear-gradient(to top right, red, lime);
  background: rgb(131,58,180);
  background: linear-gradient(90deg, rgba(131,58,180,1) 30%, rgba(253,29,29,1) 61%, rgba(252,176,69,1) 100%);
  min-height: 10vh;
  height: 85vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  margin-top: 85px;
`;

const LernReact = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  const { t } = useTranslation();

  return (
    <div className="App">
      <Header>
      <img src={logo} className="App-logo" alt="logo" />
        <p>
          {t('learnReact.header.p1')}
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('learnReact.header.linkText')}
        </a>
      </Header>
    </div>
  );
}

export default LernReact;