import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { DefaultProps } from './types/default';
import NavBar from './components/navigation/NavBar';
import Home from './pages/Home';
import LearnReact from './pages/LearnReact';
import About from './pages/About';
import NotFoundPage from './pages/NotFoundPage';

const Router = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {

//See: https://create-react-app.dev/docs/deployment/#building-for-relative-paths 
  return (
    <BrowserRouter basename="/react/react-example">
      <NavBar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/learn-react" component={LearnReact} />
        <Route path="/about" component={About} />
        <Route component={NotFoundPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default Router;