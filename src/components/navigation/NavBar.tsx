import React, { useState, SyntheticEvent } from 'react';
import { DefaultProps } from '../../types/default';
import { useTranslation } from 'react-i18next';
import styled  from 'styled-components';
import { Navbar, Nav } from 'react-bootstrap';
import NavLinkButton from './NavLinkButton';
import NavLangSelector from './NavLangSelector';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../reducers/appReducer';
import { setActiveNavButtonId } from '../../actions/appActions';

/**
 * Styles
 */
const NavBarContainer = styled(Navbar)`
  width: 100%;
  /*height: 85px;
  max-height: 500px;*/
`;

const NavItems = styled(Nav)`
  width: 85%;
`;
const NavLangSelection = styled(NavLangSelector)`
  /*text-align: right;*/
`;
const BrandText = styled.span`
  font-family: "Chilanka";
  font-size: 1.5em;
  font-weight: bold;
  vertical-align: middle;
  padding-top: 1em;
`;

export interface NavBarProps extends DefaultProps {
  lang: string;
}

/**
 * NavBar component
 * @param props lang - "en" | "de"
 */
const NavBar = (props: NavBarProps): React.FunctionComponentElement<NavBarProps> => {
  const { t } = useTranslation();
  //const [activeNavLinkButtonId, setActiveNavLinkButtonId] = useState("home");
  const [activeNavLinkButtons, setActiveNavLinkButtons] = useState([false, false, false]);
  const [navBarExpanded, setNavBarExpanded] = useState(false);
  

  const dispatch = useDispatch();
  //const activeNavButtonId = useSelector((state: { app: AppState}) => state.app.activeNavButtonId);

  const onNavLinkButtonClick = (id: string) => {
    //setActiveNavLinkButtonId(id);
    dispatch(setActiveNavButtonId(id));
    switch(id){
      case "home": setActiveNavLinkButtons([true, false, false]); break;
      case "learnReact": setActiveNavLinkButtons([false, true, false]); break;
      case "about": setActiveNavLinkButtons([false, false, true]); break;
      default: setActiveNavLinkButtons([false, false, false]);
    }
    /* collapse NavBar with a delay */
    setTimeout(() => {setNavBarExpanded(false)}, 250);
  }

  return (
    <NavBarContainer collapseOnSelect expanded={navBarExpanded} navbar-default expand="lg" bg="dark" variant="dark" fixed="top" >
      <Navbar.Brand href="/"><BrandText>React Example</BrandText></Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={() => setNavBarExpanded(navBarExpanded ? false : true)} />
      <Navbar.Collapse id="responsive-navbar-nav">
        <NavItems className="mr-auto" variant="pills" defaultActiveKey="/">
          <Nav.Item>
            <NavLinkButton id="home" to="/" active={activeNavLinkButtons[0]} onClick={onNavLinkButtonClick}>{t('home.title')}</NavLinkButton>
          </Nav.Item>
          <Nav.Item>
            <NavLinkButton id="learnReact" to="/learn-react" active={activeNavLinkButtons[1]} onClick={onNavLinkButtonClick}>{t('learnReact.title')}</NavLinkButton>
          </Nav.Item>
          <Nav.Item>
            <NavLinkButton id="about" to="/about" active={activeNavLinkButtons[2]} onClick={onNavLinkButtonClick}>{t('about.title')}</NavLinkButton>
          </Nav.Item>
        </NavItems>
        <NavLangSelection value={props.lang} onSelected={(lang) => setTimeout(() => {setNavBarExpanded(false)}, 450)}/>
      </Navbar.Collapse>
    </NavBarContainer>
  );
}

/**
 * Default component props
 */
NavBar.defaultProps = { lang: "de" }

export default NavBar;