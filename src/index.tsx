import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Router from './router';
import I18nGate from './i18nGate';
import './index.css';
import { PersistGate } from 'redux-persist/lib/integration/react';
import configureStore from './configureStore';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

const { store, persistor } = configureStore();

const routing = (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <I18nGate>
        <Router />
      </I18nGate>
    </PersistGate>
  </Provider>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
