import { createStore, Store } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { Persistor } from 'redux-persist/es/types';
import storage from 'redux-persist/lib/storage';
import rootReducer from './reducers';

interface ConfigureStore {
  store: Store;
  persistor: Persistor;
}

const persistConfig = {
  key: 'root',
  storage: storage,
 };

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default (): ConfigureStore => {
  const store: Store = createStore(persistedReducer, /*preloadedState,*/
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());
  const persistor: Persistor = persistStore(store);
  return { store, persistor };
};
