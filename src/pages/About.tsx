import React from 'react';
import { useTranslation } from 'react-i18next';
import { DefaultProps } from '../types/default';
import styled  from 'styled-components';

const Container = styled.div`
  text-align: center;
`;

const Header = styled.header`
  background-color: #282c34;
  background: linear-gradient(to top right, red, lime);
  background: rgb(131,58,180);
  background: linear-gradient(90deg, rgba(131,58,180,1) 30%, rgba(253,29,29,1) 61%, rgba(252,176,69,1) 100%);
  min-height: 10vh;
  height: 15vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  margin-top: 85px;
`;

const Headline = styled.h1`
    color: white;
    text-decoration: underline;
    font-size: 8vh;
  `;

const Content = styled.main`
  background-color: white;
  height: 70vh;
`;

const About = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  const { t } = useTranslation();

  return (
    <Container>
      <Header>
        <Headline>{t('about.header.h1')}</Headline>
      </Header>
      <Content>
        <h2>{t('about.body.text')}...</h2>
      </Content>
    </Container>
  );
}

export default About;