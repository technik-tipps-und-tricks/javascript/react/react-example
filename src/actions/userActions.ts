export const SET_FIRST_NAME = 'USER/SET_FIRST_NAME';
export const SET_LAST_NAME = 'USER/SET_LAST_NAME';

 export type UserActionTypes = SetFirstName | SetLastName;

 interface SetFirstName {
  type: typeof SET_FIRST_NAME;
  payload: SetFirstNamePayload;
}

 interface SetFirstNamePayload {
  firstName: string;
}

 interface SetLastName {
  type: typeof SET_LAST_NAME;
  payload: SetLastNamePayload;
}

 interface SetLastNamePayload {
  lastName: string;
}

 export function setFirstName(firstName: string): SetFirstName {
  return {
    type: SET_FIRST_NAME,
    payload: {
      firstName,
    },
  };
}

 export function setLastName(lastName: string): SetLastName {
  return {
    type: SET_LAST_NAME,
    payload: {
      lastName,
    },
  };
}