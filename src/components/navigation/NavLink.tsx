import React from 'react';
import { DefaultProps } from '../../types/default';
import { Link } from "react-router-dom";
import { Nav } from "react-bootstrap";

export interface NavLinkProps extends DefaultProps {
  to: string;
}

/**
 * NavLink component
 * @param props to - navigation route link as string
 */
const NavLink = (props: NavLinkProps): React.FunctionComponentElement<NavLinkProps> => {
  
  return (
    <Nav.Link as={Link} to={props.to}>
      {props.children}
    </Nav.Link>
  );
}

/**
 * Default component props.
 */
NavLink.defaultProps = {
  children: null,
  to: "/",
}
export default NavLink;