import { SET_FIRST_NAME, SET_LAST_NAME, UserActionTypes } from '../actions/userActions';

 export interface UserState {
  firstName: string;
  lastName: string;
}

 const initialState: UserState = {
  firstName: 'Max',
  lastName: 'Mustermann',
};

 const userReducer = (state = initialState, action: UserActionTypes): UserState => {
  switch (action.type) {
    case SET_FIRST_NAME:
      return {
        ...state,firstName: action.payload.firstName,
      };
    case SET_LAST_NAME:
      return {
        ...state,
        lastName: action.payload.lastName,
      };
    default:
      return state;
  }
};

 export default userReducer;