import React from 'react';
import { DefaultProps } from '../types/default';
import { useTranslation } from 'react-i18next';
import styled  from 'styled-components';
import { Image } from 'react-bootstrap';
import { Z_BLOCK } from 'zlib';

const Container = styled.div`
  text-align: center;
`;

const Header = styled.header`
  background-color: #282c34;
  background: linear-gradient(to top right, red, lime);
  background: rgb(131,58,180);
  background: linear-gradient(90deg, rgba(131,58,180,1) 30%, rgba(253,29,29,1) 61%, rgba(252,176,69,1) 100%);
  min-height: 10vh;
  height: 15vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  margin-top: 85px;
`;

const Headline = styled.h1`
    color: white;
    text-decoration: underline;
    font-size: 8vh;
  `;

const Content = styled.main`
  background-color: gray;
  height: 70vh;
`;

const Aside = styled.aside`
  float: left; 
  background-color: white;
  width: 20vw;
  height: 70vh;
`;

const AsideHeadline = styled.h1`
  font-size: calc(2.3vw + 2.3vh);
`;

const Home = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  const { t } = useTranslation();
  
  return (
    <Container>
      <Header>
        <Headline>{t('home.header.h1')}</Headline>
      </Header>
      <Aside>
        <AsideHeadline>{t('home.sidebar.title')}</AsideHeadline>
        <ul>
          <li>{t('home.sidebar.point')} 1</li>
          <li>{t('home.sidebar.point')} 2</li>
        </ul>
      </Aside>
      <Content>
        <h1>{t('home.body.chapter')} 1</h1>
        <p>{t('home.body.text')}</p>
        
      </Content>
      
    </Container>
  );
}

export default Home;